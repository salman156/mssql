CREATE DATABASE db;

USE db;

CREATE TABLE products(
  id INTEGER NOT NULL,
  name VARCHAR(64) NOT NULL,
  CONSTRAINT PK_product_id PRIMARY KEY (id)
);
 
CREATE TABLE categories(
  id INTEGER NOT NULL,
  name VARCHAR(64) NOT NULL,
  CONSTRAINT PK_category_id PRIMARY KEY (id)
);

CREATE TABLE product_categories (
    product_id INTEGER NOT NULL,
    category_id INTEGER NOT NULL,
    CONSTRAINT FK_Product_id FOREIGN KEY (product_id)
        REFERENCES products (id),
    CONSTRAINT FK_category_id FOREIGN KEY (category_id)
        REFERENCES categories (id)
)

INSERT INTO products (id, name) VALUES
(1, 'TV'),
(2, 'Telephone'),
(3, 'Tide'),
(4, 'Pants');

INSERT INTO categories (id, name) VALUES
(1, 'Electronics'),
(2, 'Chemicals');

INSERT INTO product_categories (product_id, category_id) VALUES
(1, 1),
(2, 1),
(3,2);

SELECT p.name, c.name FROM product_categories pc
INNER JOIN categories c ON category_id = c.id
RIGHT JOIN products p ON p.id = product_id;